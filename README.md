# README #

### What is this repository for? ###

* Quick summary
Retrieves and analyzes expired domains for residual value. This can be in the form of backlinks or content which will be orphaned and no longer be available.


### How do I get set up? ###

* Git clone
* Run composer
* Dependencies
Uses symfony and node / gulp for frontend build tasks
* How to run tests
PHPUnit at the root of the repo.
* Deployment instructions
Git clone and run composer. Gulp and associated dependencies will also be needed.


### Who do I talk to? ###

* christo@gameofcode.co.uk