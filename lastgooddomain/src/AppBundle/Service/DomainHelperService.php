<?php

namespace AppBundle\Service;

class DomainHelperService
{
    public static function getLength($domain_name){
        return strlen($domain_name);
    }

    public static function getExtension($domain_name){
        $result = explode('.', $domain_name);

        if (sizeof($result) > 2){
            return '.' .$result[1]. '.' .$result[2];
        }
        else{
            return $result[1];
        }
    }

    public static function hasHyphens($domain_name){
        if (strpos($domain_name, '-')){
            return true;
        }
        return false;
    }

    public static function hasNumbers($domain_name){

        if (preg_match( '/[0-9]/', $domain_name) > 0){
            return true;
        }
        return false;
    }

}
