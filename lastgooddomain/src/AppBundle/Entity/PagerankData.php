<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PagerankData
 *
 * @ORM\Table(name="pagerank_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PagerankDataRepository")
 */
class PagerankData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="domain_id", type="integer", unique=true)
     */
    private $domainId;

    /**
     * @var int
     *
     * @ORM\Column(name="pagerank", type="integer")
     */
    private $pagerank;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checked_date", type="datetime")
     */
    private $checkedDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domainId
     *
     * @param integer $domainId
     *
     * @return PagerankData
     */
    public function setDomainId($domainId)
    {
        $this->domainId = $domainId;

        return $this;
    }

    /**
     * Get domainId
     *
     * @return int
     */
    public function getDomainId()
    {
        return $this->domainId;
    }

    /**
     * Set pagerank
     *
     * @param integer $pagerank
     *
     * @return PagerankData
     */
    public function setPagerank($pagerank)
    {
        $this->pagerank = $pagerank;

        return $this;
    }

    /**
     * Get pagerank
     *
     * @return int
     */
    public function getPagerank()
    {
        return $this->pagerank;
    }

    /**
     * Set checkedDate
     *
     * @param \DateTime $checkedDate
     *
     * @return PagerankData
     */
    public function setCheckedDate($checkedDate)
    {
        $this->checkedDate = $checkedDate;

        return $this;
    }

    /**
     * Get checkedDate
     *
     * @return \DateTime
     */
    public function getCheckedDate()
    {
        return $this->checkedDate;
    }
}
