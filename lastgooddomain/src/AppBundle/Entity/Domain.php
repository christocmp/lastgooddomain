<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Domain
 *
 * @ORM\Table(name="domain")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DomainRepository")
 */
class Domain
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="domain_name", type="string", length=255, unique=true)
     */
    private $domainName;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=20)
     */
    private $extension;

    /**
     * @var int
     *
     * @ORM\Column(name="length", type="integer")
     */
    private $length;

    /**
     * @var string
     *
     * @ORM\Column(name="words", type="string", length=255)
     */
    private $words;

    /**
     * @var int
     *
     * @ORM\Column(name="word_count", type="integer")
     */
    private $wordCount;

    /**
     * @var bool
     *
     * @ORM\Column(name="hyphens", type="boolean")
     */
    private $hyphens;

    /**
     * @var bool
     *
     * @ORM\Column(name="numbers", type="boolean")
     */
    private $numbers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime")
     */
    private $dateAdded;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domainName
     *
     * @param string $domainName
     *
     * @return Domain
     */
    public function setDomainName($domainName)
    {
        $this->domainName = $domainName;

        return $this;
    }

    /**
     * Get domainName
     *
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return Domain
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set length
     *
     * @param integer $length
     *
     * @return Domain
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set words
     *
     * @param string $words
     *
     * @return Domain
     */
    public function setWords($words)
    {
        $this->words = $words;

        return $this;
    }

    /**
     * Get words
     *
     * @return string
     */
    public function getWords()
    {
        return $this->words;
    }

    /**
     * Set wordCount
     *
     * @param integer $wordCount
     *
     * @return Domain
     */
    public function setWordCount($wordCount)
    {
        $this->wordCount = $wordCount;

        return $this;
    }

    /**
     * Get wordCount
     *
     * @return int
     */
    public function getWordCount()
    {
        return $this->wordCount;
    }

    /**
     * Set hyphens
     *
     * @param boolean $hyphens
     *
     * @return Domain
     */
    public function setHyphens($hyphens)
    {
        $this->hyphens = $hyphens;

        return $this;
    }

    /**
     * Get hyphens
     *
     * @return bool
     */
    public function getHyphens()
    {
        return $this->hyphens;
    }

    /**
     * Set numbers
     *
     * @param boolean $numbers
     *
     * @return Domain
     */
    public function setNumbers($numbers)
    {
        $this->numbers = $numbers;

        return $this;
    }

    /**
     * Get numbers
     *
     * @return bool
     */
    public function getNumbers()
    {
        return $this->numbers;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Domain
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }
}
