<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * SnapnamesSaledata
 *
 * @ORM\Table(name="snapnames_saledata")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SnapnamesSaledataRepository")
 */
class SnapnamesSaledata
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="domain_id", type="integer")
     */
    private $domainId;

    /**
     * @var string
     *
     * @ORM\Column(name="current_bid", type="decimal", precision=10, scale=0)
     */
    private $currentBid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="join_by_date", type="datetime")
     */
    private $joinByDate;

    /**
     * @var int
     *
     * @ORM\Column(name="bidders", type="integer")
     */
    private $bidders;

    /**
     * @var string
     *
     * @ORM\Column(name="seller", type="string", length=255)
     */
    private $seller;

    /**
     * @var string
     *
     * @ORM\Column(name="auction_type", type="string", length=255)
     */
    private $auctionType;


    /**
     * @ORM\OneToOne(targetEntity="Domain")
     * @ORM\JoinColumn(name="domain_id", referencedColumnName="id")
     */

    private $domains;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domainId
     *
     * @param integer $domainId
     *
     * @return SnapnamesSaledata
     */
    public function setDomainId($domainId)
    {
        $this->domainId = $domainId;

        return $this;
    }

    /**
     * Get domainId
     *
     * @return int
     */
    public function getDomainId()
    {
        return $this->domainId;
    }

    /**
     * Set currentBid
     *
     * @param string $currentBid
     *
     * @return SnapnamesSaledata
     */
    public function setCurrentBid($currentBid)
    {
        $this->currentBid = $currentBid;

        return $this;
    }

    /**
     * Get currentBid
     *
     * @return string
     */
    public function getCurrentBid()
    {
        return $this->currentBid;
    }

    /**
     * Set joinByDate
     *
     * @param \DateTime $joinByDate
     *
     * @return SnapnamesSaledata
     */
    public function setJoinByDate($joinByDate)
    {
        $this->joinByDate = $joinByDate;

        return $this;
    }

    /**
     * Get joinByDate
     *
     * @return \DateTime
     */
    public function getJoinByDate()
    {
        return $this->joinByDate;
    }

    /**
     * Set bidders
     *
     * @param integer $bidders
     *
     * @return SnapnamesSaledata
     */
    public function setBidders($bidders)
    {
        $this->bidders = $bidders;

        return $this;
    }

    /**
     * Get bidders
     *
     * @return int
     */
    public function getBidders()
    {
        return $this->bidders;
    }

    /**
     * Set seller
     *
     * @param string $seller
     *
     * @return SnapnamesSaledata
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * Get seller
     *
     * @return string
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * Set auctionType
     *
     * @param string $auctionType
     *
     * @return SnapnamesSaledata
     */
    public function setAuctionType($auctionType)
    {
        $this->auctionType = $auctionType;

        return $this;
    }

    /**
     * Get auctionType
     *
     * @return string
     */
    public function getAuctionType()
    {
        return $this->auctionType;
    }


    /**
     * Set domains
     *
     * @param \AppBundle\Entity\Domain $domains
     *
     * @return SnapnamesSaledata
     */
    public function setDomains(\AppBundle\Entity\Domain $domains = null)
    {
        $this->domains = $domains;

        return $this;
    }

    /**
     * Get domains
     *
     * @return \AppBundle\Entity\Domain
     */
    public function getDomains()
    {
        return $this->domains;
    }
}
