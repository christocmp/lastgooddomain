<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AlexaData
 *
 * @ORM\Table(name="alexa_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlexaDataRepository")
 */
class AlexaData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="domain_id", type="integer", unique=true)
     */
    private $domainId;

    /**
     * @var int
     *
     * @ORM\Column(name="alexa_rank", type="integer")
     */
    private $alexaRank;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checked_date", type="datetime")
     */
    private $checkedDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domainId
     *
     * @param integer $domainId
     *
     * @return AlexaData
     */
    public function setDomainId($domainId)
    {
        $this->domainId = $domainId;

        return $this;
    }

    /**
     * Get domainId
     *
     * @return int
     */
    public function getDomainId()
    {
        return $this->domainId;
    }

    /**
     * Set alexaRank
     *
     * @param integer $alexaRank
     *
     * @return AlexaData
     */
    public function setAlexaRank($alexaRank)
    {
        $this->alexaRank = $alexaRank;

        return $this;
    }

    /**
     * Get alexaRank
     *
     * @return int
     */
    public function getAlexaRank()
    {
        return $this->alexaRank;
    }

    /**
     * Set checkedDate
     *
     * @param \DateTime $checkedDate
     *
     * @return AlexaData
     */
    public function setCheckedDate($checkedDate)
    {
        $this->checkedDate = $checkedDate;

        return $this;
    }

    /**
     * Get checkedDate
     *
     * @return \DateTime
     */
    public function getCheckedDate()
    {
        return $this->checkedDate;
    }
}
