<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\Domain;
use AppBundle\Entity\SnapnamesSaledata;
use League\Csv\Reader;
use AppBundle\Service\DomainHelperService;

class DownloadlistsCommand extends ContainerAwareCommand
{
    private $em;
    private $doctrine;
    protected function configure()
    {
        $this
            ->setName('downloadlists')
            ->setDescription('Download expired domain lists')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        if ($input->getOption('option')) {
            // ...
        }

        $this->doctrine = $this->getContainer()->get('doctrine');
        $this->em = $this->doctrine->getManager();
        $io = new SymfonyStyle($input, $output);

        $io->title('Downloading expired domain lists...');

      //  $progress = new ProgressBar($output, 100);
      //  $progress->start();

        $domain_lists = array(
            array('type' => 'exclusive', 'file' => 'snpexpiringexlusivelist.zip', 'url' => 'https://www.snapnames.com/file_dl.sn?file=snpexpiringexlusivelist.zip'),
            array('type' => 'deleted', 'file' => 'snpdeletinglist.zip', 'url' => 'https://www.snapnames.com/file_dl.sn?file=snpdeletinglist.zip'),
            array('type' => 'auction', 'file' => 'snpmostactivelist.zip', 'url' => 'https://www.snapnames.com/file_dl.sn?file=snpmostactivelist.zip')
        );


        //download and extract each file
        foreach ($domain_lists as $domain_list){

            //skip download if file is new
            if (filemtime(realpath($this->getContainer()->getParameter('kernel.root_dir')) .'/Resources/feeds/' . $domain_list['file']) > time() - 14400){
                $io->writeln($domain_list['file'] . ' is 4 hours old or newer skipping download');
                continue;
            }

            self::download($domain_list['url'], $domain_list['file']);
            $io->writeln('downloaded ' . $domain_list['file']);
            if (self::extract($domain_list['file'])){
                $io->writeln('unzipped ' . $domain_list['file'] );
            }
            else{
                $io->writeln('unzip failed');
            }

        }

        $io->writeln('');
        $io->writeln('Download complete');
        //$this->getContainer()->get('doctrine');
        $domain_repository = $this->getContainer()->get('doctrine')
            ->getRepository('AppBundle:Domain');
        // import each file
        foreach ($domain_lists as $domain_list){

            $csv = Reader::createFromPath(realpath($this->getContainer()->getParameter('kernel.root_dir')) .'/Resources/feeds/' .  str_replace('zip', 'txt', $domain_list['file']));
            $csv->setDelimiter("\t");

            $total_rows = $csv->each(function ($row) {
                return true;
            });

            $io->writeln('');
            $io->writeln('total rows ' . $total_rows);

            // if last record present list has already been processed
            $last_row = $csv->fetchOne($total_rows - 1);

            //todo if last row continue
            if ($domain_repository->findOneByDomainName($last_row[0])){
                $io->writeln('list already processed skipping...');
                continue;
            }
            else{
                $io->writeln('checking ' . $last_row[0] . ' did not find it!');
            }




            $res = $csv->setOffset(1)->setLimit(100)->fetchAll();

            $io->writeln('processing... ' . $domain_list['file']);

            $progress_list1 = new ProgressBar($io, sizeof($res));
            foreach ($res as $domain_rec){
                $progress_list1->advance();
                $io->writeln(' ' . $domain_rec[0]);


                if ($domain_repository->findOneByDomainName($domain_rec[0])){ //check if domain exists
                    $io->writeln('domain already added');
                    continue;
                }

                //upcoming domain with snapname data (no analysis)
                $domain = new Domain();

                $domain_name = trim($domain_rec[0]);
                $domain->setDomainName($domain_name);

                $domain->setExtension(DomainHelperService::getExtension($domain_name));
                $domain->setLength(DomainHelperService::getLength($domain_name));
                $domain->setWords(trim($domain_rec[8]));
                //todo word count blank in data needs to be calculated via a dictionary search on domain maybe
                /*
                print_r(explode(' ', trim($domain_rec[8])));
                $wordCount = sizeof(explode(' ', trim($domain_rec[8])));
                */

                $domain->setWordCount(0);
                $domain->setHyphens(DomainHelperService::hasHyphens($domain_name));
                $domain->setNumbers(DomainHelperService::hasNumbers($domain_name));
                $domain->setDateAdded(new \DateTime("now"));

                $this->em->persist($domain);
                $this->em->flush();

                $snapnames = new SnapnamesSaledata();
                $snapnames->setCurrentBid($domain_rec[1]);

                $join_by_date = new \DateTime($domain_rec[2], new \DateTimeZone('America/New_York'));

                $join_by_date->setTimezone(new \DateTimeZone('Europe/London'));

                $snapnames->setJoinByDate($join_by_date);
                $snapnames->setBidders($domain_rec[4]);
                $snapnames->setSeller($domain_rec[5]);
                $snapnames->setAuctionType($domain_rec[13]);
                $snapnames->setDomains($domain);
                $this->em->persist($snapnames);

            }
            $this->em->flush();
            $progress_list1->finish();

        }
        $io->success('Domain import complete');


    }

    function download($url, $file)
    {
        //destination
        $fp = fopen(realpath($this->getContainer()->getParameter('kernel.root_dir')) . '/Resources/feeds/' .  $file, 'w+');

        //source
        $ch = curl_init(str_replace(" ", "%20", $url));

        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }

    function extract($file)
    {

        $zip = new \ZipArchive;
        if ($zip->open(realpath($this->getContainer()->getParameter('kernel.root_dir')) . '/Resources/feeds/' . $file) === TRUE) {
            $zip->extractTo(realpath($this->getContainer()->getParameter('kernel.root_dir')) . '/Resources/feeds/');
            $zip->close();
            return true;
        } else {
            return false;
        }
    }



}
