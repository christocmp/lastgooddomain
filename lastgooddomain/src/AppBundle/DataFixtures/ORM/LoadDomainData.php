<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Domain;
use AppBundle\Entity\SnapnamesSaledata;
use AppBundle\Entity\PagerankData;
use AppBundle\Entity\AlexaData;
use AppBundle\Service\DomainHelperService; //semantic but probably just a dependency injection!

class LoadDomains implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //just a domain
        $domain = new Domain();
        $domain_name = 'uncheckeddomain.co.uk';
        $domain->setDomainName($domain_name);
        $domain->setExtension(DomainHelperService::getExtension($domain_name));
        $domain->setLength(DomainHelperService::getLength($domain_name));
        $domain->setWords('unchecked,domain');
        $domain->setWordCount(2);
        $domain->setHyphens(DomainHelperService::hasHyphens($domain_name));
        $domain->setNumbers(DomainHelperService::hasNumbers($domain_name));
        $domain->setDateAdded(new \DateTime("now"));

        $manager->persist($domain);

        //upcoming domain with snapname data (no analysis)
        $domain = new Domain();
        $domain_name = 'snapnamedomain.com';
        $domain->setDomainName($domain_name);

        $domain->setExtension(DomainHelperService::getExtension($domain_name));
        $domain->setLength(DomainHelperService::getLength($domain_name));
        $domain->setWords('domain');
        $domain->setWordCount(1);
        $domain->setHyphens(DomainHelperService::hasHyphens($domain_name));
        $domain->setNumbers(DomainHelperService::hasNumbers($domain_name));
        $domain->setDateAdded(new \DateTime("now"));

        $manager->persist($domain);
        $manager->flush();

        $snapnames = new SnapnamesSaledata();
        $snapnames->setCurrentBid('0.00');

        $future_date = new \DateTime("now");
        $future_date->modify("+3 days");

        $snapnames->setJoinByDate($future_date);
        $snapnames->setBidders(0);
        $snapnames->setSeller('snapnames');
        $snapnames->setAuctionType('auction');
        $snapnames->setDomains($domain);
        $manager->persist($snapnames);


        //upcoming domain with snapname data and analysis
        $domain = new Domain();
        $domain_name = 'snapnamedomainanddata.com';
        $domain->setDomainName($domain_name);

        $domain->setExtension(DomainHelperService::getExtension($domain_name));
        $domain->setLength(DomainHelperService::getLength($domain_name));
        $domain->setWords('domain,data');
        $domain->setWordCount(2);
        $domain->setHyphens(DomainHelperService::hasHyphens($domain_name));
        $domain->setNumbers(DomainHelperService::hasNumbers($domain_name));
        $domain->setDateAdded(new \DateTime("now"));

        $manager->persist($domain);
        $manager->flush();

        $domain_id = $domain->getId();
        $snapnames = new SnapnamesSaledata();

        //$snapnames->setDomainId($domain_id);
        $snapnames->setDomains($domain);
        $snapnames->setCurrentBid('0.00');

        $future_date = new \DateTime("now");
        $future_date->modify("+3 days");

        $snapnames->setJoinByDate($future_date);
        $snapnames->setBidders(0);
        $snapnames->setSeller('snapnames');
        $snapnames->setAuctionType('auction');
        $snapnames->setDomains($domain);
        $manager->persist($snapnames);

        $alexaData = new AlexaData();
        $alexaData->setDomainId($domain_id);

        $alexaData->setAlexaRank(1234567);
        $alexaData->setCheckedDate(new \DateTime("now"));

        $googleData = new PagerankData();
        $googleData->setDomainId($domain_id);
        $googleData->setPagerank(3);
        $googleData->setCheckedDate(new \DateTime("now"));

        $manager->persist($alexaData);
        $manager->persist($googleData);
        $manager->flush();


        //snapname domain with analysis but in the past
        $domain = new Domain();
        $domain_name = 'oldsnapnamedomainanddata.com';
        $domain->setDomainName($domain_name);

        $domain->setExtension(DomainHelperService::getExtension($domain_name));
        $domain->setLength(DomainHelperService::getLength($domain_name));
        $domain->setWords('old,domain,data');
        $domain->setWordCount(3);
        $domain->setHyphens(DomainHelperService::hasHyphens($domain_name));
        $domain->setNumbers(DomainHelperService::hasNumbers($domain_name));
        $domain->setDateAdded(new \DateTime("now"));

        $manager->persist($domain);
        $manager->flush();

        $domain_id = $domain->getId();
        $snapnames = new SnapnamesSaledata();

        //$snapnames->setDomainId($domain_id);
        $snapnames->setCurrentBid('0.00');

        $future_date = new \DateTime("now");
        $future_date->modify("-3 days");

        $snapnames->setJoinByDate($future_date);
        $snapnames->setBidders(0);
        $snapnames->setSeller('snapnames');
        $snapnames->setAuctionType('auction');
        $snapnames->setDomains($domain);
        $manager->persist($snapnames);

        $alexaData = new AlexaData();
        $alexaData->setDomainId($domain_id);

        $alexaData->setAlexaRank(1234567);
        $alexaData->setCheckedDate(new \DateTime("now"));

        $googleData = new PagerankData();
        $googleData->setDomainId($domain_id);
        $googleData->setPagerank(3);
        $googleData->setCheckedDate(new \DateTime("now"));

        $manager->persist($alexaData);
        $manager->persist($googleData);
        $manager->flush();

    }

}
