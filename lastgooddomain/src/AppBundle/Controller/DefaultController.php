<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Domain;
use AppBundle\Entity\SnapnamesSaledata;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('search_term', SearchType::class)
            ->add('search', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated

            print_r($form->getData());
            die();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            // $em = $this->getDoctrine()->getManager();
            // $em->persist($task);
            // $em->flush();

            return $this->redirectToRoute('task_success');
        }




        return $this->render('default.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'search_form' => $form->createView(),
            'pathInfo' => $request->getPathInfo() //for active menu item
        ]);
    }

    /**
     * @Route("/summary", name="summary")
     */
    public function summaryAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'pathInfo' => $request->getPathInfo() //for active menu item
        ]);
    }

    /**
     * @Route("/download", name="download")
     */
    public function downloadAction(Request $request)
    {
        $domain_lists = array(
            array('type' => 'exclusive', 'file' => 'snpexpiringexlusivelist.zip', 'url' => 'https://www.snapnames.com/file_dl.sn?file=snpexpiringexlusivelist.zip'),
            array('type' => 'deleted', 'file' => 'snpdeletinglist.zip', 'url' => 'https://www.snapnames.com/file_dl.sn?file=snpdeletinglist.zip'),
            array('type' => 'auction', 'file' => 'snpmostactivelist.zip', 'url' => 'https://www.snapnames.com/file_dl.sn?file=snpmostactivelist.zip')
        );

        foreach ($domain_lists as $domain_list){
            echo $domain_list['file'] . '<br />';
        }
        return;

        //destination
        $fp = fopen (realpath($this->getParameter('kernel.root_dir')) .'/Resources/feeds/snpexpiringexlusivelist.zip', 'w+');

        //source
        $ch = curl_init(str_replace(" ","%20",'https://www.snapnames.com/file_dl.sn?file=snpexpiringexlusivelist.zip'));


        curl_setopt($ch, CURLOPT_TIMEOUT, 50);

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        $zip = new \ZipArchive;
        if ($zip->open(realpath($this->getParameter('kernel.root_dir')) .'/Resources/feeds/snpexpiringexlusivelist.zip') === TRUE) {
            $zip->extractTo(realpath($this->getParameter('kernel.root_dir')) .'/Resources/feeds/');
            $zip->close();
            echo 'ok';
        } else {
            echo 'failed';
        }

    }



    /**
     * @Route("/import", name="import")
     */

    public function importSnapnamesLists()
    {
        echo "importing lists \n";
        flush();

        $domain_feed_files = array();
        echo "reading snapnames files \n";
        echo realpath($this->getParameter('kernel.root_dir')) .'/Resources/feeds/';
        flush();
        $directory = dir(realpath($this->getParameter('kernel.root_dir')) .'/Resources/feeds/');

        while (false !== ($f = $directory->read())) {
            if (is_file($directory->path.'/'.$f) && strpos($f, '.txt')) {
                $domain_feed_files[] = $f;
            }
        }
        $directory->close();

        print_r ($domain_feed_files);

        echo "begining database import \n";
        flush();
        $row = 1;
        foreach ($domain_feed_files as $file){
            $handle = fopen($file, "r");
            while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {


                if ($row == 1){
                    $row++;
                    continue;
                }

                $domain_name = trim($data[0]);
//		echo "$domain_name \n";
//                flush();

                if ($domain->isDuplicate($domain_name)){
//                        echo "duplicate \n";
//                        flush();
                    continue;
                }

                $current_bid = $data[1];
                $phpdate = strtotime( $data[2] );
                $join_date =   date( 'Y-m-d', $phpdate );

                if ($data[4] == ''){
                    $bidders = 0;
                }
                else{
                    $bidders = $data[4];
                }

                $seller =      trim($data[5]);
                $extension =   trim($data[6]);
                $length =      $data[7];
                $keywords    = trim($data[8]);
                $word_count  = trim($data[9]);
                $categories  = trim($data[10]);

                if ($data[11] == 'Yes'){
                    $hyphens = 1;
                }
                else{
                    $hyphens = 0;
                }

                if ($data[12] == 'Yes'){
                    $numbers = 1;
                }
                else{
                    $numbers = 0;
                }

                $auction_type = trim($data[13]);

                $category1 = '';
                $category2 = '';
                $category3 = '';

                $pieces = explode(" ", $categories);

                if (isset($pieces[0]) && !empty($pieces[0])){
                    $category1 = $pieces[0];
                    if (isset($pieces[1]) && !empty($pieces[1])){
                        $category2 = $pieces[1];
                    }
                    if (isset($pieces[2]) && !empty($pieces[2])){
                        $category3 = $pieces[2];
                    }
                }

                $domain->addDomain($domain_name, $current_bid, $join_date, $bidders, $seller, $extension, $length,$keywords,$word_count,
                    $category1,$category2,$category3,$hyphens,$numbers,$auction_type);



                $row++;

            }
            fclose($handle);


        }
        echo 'import complete\n';
    }
}
