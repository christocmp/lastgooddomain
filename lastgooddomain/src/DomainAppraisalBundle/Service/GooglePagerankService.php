<?php

namespace DomainAppraisalBundle\Service;

//DEFUNCT : google no longer provide useful PR data
class GooglePagerankService
{
// Google PageRank API check entry point
    public $google_pr_api = 'http://toolbarqueries.google.com/tbr?client=navclient-auto&ch=%s&features=Rank&q=info:%s&num=100&filter=0';

    // timeout for curl requests too long. see init_curl
    public $timeout;

    // GooglePR singleton instance
    private static $instance;

    // static handler for getting pr string for url
    function get($url, $timeout = 10) {
        if(!isset(self::$instance)) {
            self::$instance = new GooglePagerankService($timeout);
        }
        return trim(substr(self::$instance->get_pr($url), 9));
    }

    // static handler for checking page existence
    function exists($url, $timeout = 10) {
        if(!isset(self::$instance)) {
            self::$instance = new GooglePagerankService($timeout);
        }
        return self::$instance->site_exists($url);
    }

    // static handler for getting page outbound links count
    function links($url, $timeout = 5) {
        if(!isset(self::$instance)) {
            self::$instance = new GooglePagerankService($timeout);
        }
        return self::$instance->get_outbound_links($url);
    }

    // class constructor with timeout argument.
    // using 10 seconds to keep flow smooth instead of default 30 that might be set by your system
    function __construct($timeout = 10) {
        $this->timeout = $timeout;
    }

    // init curl resource handle
    function init_curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($this->timeout > 0) curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        return $ch;
    }

    // dummy callback method for site exists check
    function curl_header_callback($ch, $header) {}

    // check if site exists

    function site_exists($url) {
        $ch = $this->init_curl($url);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, array($this, 'curl_header_callback'));
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        # follow only 5 redirects at max
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return !($code != 200 && $code != 302 && $code != 304);
    }

    // get pagerank
    function get_pr($url) {
        global $google_pr_api;
        $checksum = $this->check_hash($this->create_hash($url));
        $url = sprintf($this->google_pr_api, $checksum, urlencode($url));
        $ch = $this->init_curl($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        # http referer is possibly found if script is used from browser
        curl_setopt($ch, CURLOPT_REFERER, @$_SERVER['HTTP_REFERER']);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; GoogleToolbar 2.0.114-big; Windows XP 5.1)');
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    // get page content
    function get_content($url) {
        $ch = $this->init_curl($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        # follow only 5 redirects at max
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    // get outbound links stat for given page
    function get_outbound_links($url) {
        # if content is not found, return FALSE
        if (!($content = $this->get_content($url))) return FALSE;
        # find all anchor links from content
        $regexp = '<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>';
        preg_match_all("/$regexp/siU", $content, $match, PREG_SET_ORDER);
        $parsed_url = parse_url($url);
        # counter for links
        $count = 0;
        # allow only http and https schemes, not mailto: feed: or other
        $allowed_schemes = array('http', 'https');
        # NOTE: this counts outbounding hosts, not individual pages
        $added_hosts = array();
        foreach ($match as $m) {
            # some links may have '' wrappers because people tend to write bad html
            if ($matched_url = trim($m[2], "'")) {
                # get host for outbound link
                $parsed_matched_url = parse_url($matched_url);
                # if no host OR link host is same as url host OR item already handled OR scheme
                # is anything else than http/https skip host
                if (($parsed_matched_host = @$parsed_matched_url['host']) &&
                    $parsed_url['host'] != $parsed_matched_host &&
                    !in_array($parsed_matched_host, $added_hosts) &&
                    in_array($parsed_matched_url['scheme'], $allowed_schemes)) {
                    $added_hosts[] = $parsed_matched_host;
                    $count++;
                }
            }
        }
        return $count;
    }

    // convert string to a number
    function strtonmbr($string, $check, $magic) {
        $int32 = 4294967296;
        $length = strlen($string);
        for ($i = 0; $i < $length; $i++) {
            $check *= $magic;
            if ($check >= $int32) {
                $check = ($check - $int32 * (int) ($check / $int32));
                $check = ($check < -($int32 / 2)) ? ($check + $int32) : $check;
            }
            $check += ord($string{$i});
        }
        return $check;
    }

    // create a url hash
    function create_hash($string) {
        $check1 = $this->strtonmbr($string, 0x1505, 0x21);
        $check2 = $this->strtonmbr($string, 0, 0x1003F);
        $factor = 4;
        $halfFactor = $factor/2;
        $check1 >>= $halfFactor;
        $check1 = (($check1 >> $factor) & 0x3FFFFC0 ) | ($check1 & 0x3F);
        $check1 = (($check1 >> $factor) & 0x3FFC00 ) | ($check1 & 0x3FF);
        $check1 = (($check1 >> $factor) & 0x3C000 ) | ($check1 & 0x3FFF);
        $calc1 = (((($check1 & 0x3C0) << $factor) | ($check1 & 0x3C)) << $halfFactor ) | ($check2 & 0xF0F );
        $calc2 = (((($check1 & 0xFFFFC000) << $factor) | ($check1 & 0x3C00)) << 0xA) | ($check2 & 0xF0F0000 );
        return ($calc1 | $calc2);
    }

    // create checksum for hash
    function check_hash($hashNumber) {
        $check = $flag = 0;
        $hashString = sprintf('%u', $hashNumber);
        for ($i = strlen($hashString) - 1;  $i >= 0;  $i --) {
            $r = $hashString{$i};
            if (1 === ($flag % 2)) {
                $r += $r;
                $r = (int)($r / 10) + ($r % 10);
            }
            $check += $r;
            $flag++;
        }
        $check %= 10;
        if (0 !== $check) {
            $check = 10 - $check;
            if (1 === ($flag % 2) ) {
                if (1 === ($check % 2)) {
                    $check += 9;
                }
                $check >>= 1;
            }
        }
        return '7'.$check.$hashString;
    }

}
