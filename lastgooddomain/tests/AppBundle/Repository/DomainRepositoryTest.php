<?php

namespace Tests\AppBundle\Repository;

use AppBundle\Repository\DomainRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use AppBundle\Entity\Domain;


class DomainRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */

    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }

    public function testGetLatestDomains()
    {
        $domain_results = $this->em
                        ->getRepository('AppBundle:Domain')
                        ->findByExtension('com');


        //print_r($domain_results);

        foreach ($domain_results as $domain){
           echo $domain->getDomainName();

        }

        $this->assertCount(1, $domain_results);


    }

}