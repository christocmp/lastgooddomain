<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\DomainHelperService;

class DomainHelperServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testGetLength()
    {
        $length = DomainHelperService::getLength('domain.co.uk');
        $this->assertEquals($length,12);
    }

    public function testgetExtension()
    {
        $extension = DomainHelperService::getExtension('domain.co.uk');
        $this->assertEquals($extension, '.co.uk');
    }

    public function testHasHyphens()
    {
        $no_hyphens = DomainHelperService::hasHyphens('domain.co.uk');
        $this->assertEquals($no_hyphens,false);

        $with_hyphens = DomainHelperService::hasHyphens('domain-today.co.uk');
        $this->assertEquals($with_hyphens,true);
    }

    public function testHasNumbers()
    {
        $no_numbers = DomainHelperService::hasNumbers('domain.co.uk');
        $this->assertEquals($no_numbers,false);

        $with_numbers = DomainHelperService::hasNumbers('domain1-today1.co.uk');
        $this->assertEquals($with_numbers,true);
    }
}